To use ruff linter and formatter in the project follow steps bellow:
- Install pre-commit using ```python -m pip install pre-commit``` command.
- Set up pre-commit with ```python -m pip pre_commit``` command.
- That's all - now you can commit with pre-commit using default git commands.
